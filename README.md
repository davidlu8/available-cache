# available-cache

#### 介绍
Available Cache, 兼容Laravel5.6，提供两种缓存策略，simple仅支持一级缓存，l2支持二级缓存策略

#### 安装教程
composer require davidlu8/available-cache: *

如果找不到该库，可以在composer.json文件中添加下面的仓库
```
{
    "type":"git",
    "url":"https://gitee.com/davidlu8/available-cache"
}
```


#### 使用说明
* 请先将AvailableCacheServiceProvider加入到Laravel中
* 使用AvailableCache

```
AvailableCache::simple('key', function () {
    return 'data';
})

AvailableCache::l2('key', function () {
    return 'data';
})
```

#### params参数说明

* simple方法

 expire_interval 过期时间(秒) 默认600秒

* l2方法

l1_expire_interval 一级缓存过期时间(秒) 默认600秒

l2_expire_interval 二级缓存过期时间(秒) 默认86400秒

cache_lock_interval 更新二级缓存锁过期时间(秒) 默认180秒

alarm_callable 警告回调函数 默认Null

probe 记录debug日志