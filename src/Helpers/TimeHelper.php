<?php

namespace AvailableCache\Helpers;

use DateInterval;

class TimeHelper
{
    protected static $mockMode = false;

    /**
     * User: Luw
     * Datetime: 2020/6/20 17:51
     * @param bool $mode
     * @return bool
     */
    public static function setMockMode($mode)
    {
        static::$mockMode = $mode;
        return true;
    }

    /**
     * User: Luw
     * Datetime: 2020/6/20 16:03
     * @param $second
     */
    public static function getDateIntervalFromSecond($second)
    {
        if (self::$mockMode) {
            return $second;
        } else {
            return new DateInterval(sprintf('PT%sS', $second));
        }
    }

}